package com.sensicardiac.zyto.zyto_state_HRV.detector;

public class SysPulse extends Gauss_function {
	public SysPulse(double arr[]){
		super(arr);
		
	}
	protected void fit(double arr[])
	{

		height=-1;
		mean=0;
		for(int i=0;i<arr.length;i++)
		{
			if(arr[i]>height)
			{
				height=arr[i];
				mean=i;
			}
		}
		if(height==0)
		{
			height=1;
		}
		//find standard deviation
		std_deviation=0;
		for(int i=0;i<arr.length;i++)
		{
		std_deviation+=Math.pow(i-mean, 2)*arr[i]/height;
		}
		std_deviation=Math.sqrt(std_deviation);
	}
}
