package com.sensicardiac.zyto.zyto_state_HRV.detector;

public class SSF {
	private int length;
	private double old_sum;
	private double x_old;
	public SSF(int length) {
		
		this.length = length;
		reset();
	}
	
	public double add_val(double x)
	{
		double delta_x=x-x_old;
		x_old=x;
		
		if(delta_x<=0)
		{
			delta_x=0;
		}
		old_sum=old_sum+delta_x-(old_sum/length);
		return old_sum;
	}
	public double[] array_process(double[] arr)
	{
		double out[]=new double[arr.length];
		reset();
		for(int i=0;i<arr.length;i++)
		{
			out[i]=add_val(arr[i]);
		}
		return out;
	}
	public double[] array_process(int[] arr)
	{
		double out[]=new double[arr.length];
		reset();
		for(int i=0;i<arr.length;i++)
		{
			out[i]=add_val(arr[i]);
		}
		return out;
	}
	public void reset()
	{
		old_sum=0;
		x_old=0;
	}
	

}
