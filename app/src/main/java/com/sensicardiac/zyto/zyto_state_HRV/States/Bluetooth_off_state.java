package com.sensicardiac.zyto.zyto_state_HRV.States;


import com.sensicardiac.zyto.zyto_state_HRV.Variables.Globals;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class Bluetooth_off_state extends DriverState {

	@Override
	public String getStateName() {
		// TODO Auto-generated method stub
		return "bluetooth_off";
	}

	@Override
	public void onEntry() {
		// TODO Auto-generated method stub
		//Check to see if bluetooth is on.
		if(Globals.mainActivity.isBluetoothEnabled())
		{
			Globals.driverStateManager.setState(new Bluetooth_on_state());
		}
		
	}

	@Override
	public void onExit() {
		// TODO Auto-generated method stub
		
	}

	public void start_bluetooth()
	{
		Globals.mainActivity.turnOnBluetooth();
		//Globals.driverStateManager.setState(new Bluetooth_on_state());
	}
	public void recieve_message(String text) {
		Object obj = null;

        try {
            obj = Globals.parser.parse(text);
            JSONObject jsonObject = (JSONObject) obj;
            String command=(String)jsonObject.get("command");
            switch(command)
            {
                
                case "start_bluetooth":
                	//Turn on bluetooth
                	start_bluetooth();
                    break;
                case "get_state":
                	get_state();
                	break;
                    
                default:
                	Globals.webSocket_server.sendError("Not a valid command for state");
                	break;
                	

            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
	}
	@Override
	public void stop_bluetooth()
	{
//do nothing
	}
	
	@Override
	public void web_socketClose() {
		// TODO Auto-generated method stub
		Globals.driverStateManager.setState(new Unconnected_state());
		
	}

	

}
