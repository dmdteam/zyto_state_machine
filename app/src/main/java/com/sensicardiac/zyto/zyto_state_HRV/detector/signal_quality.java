package com.sensicardiac.zyto.zyto_state_HRV.detector;

import com.sensicardiac.zyto.zyto_state_HRV.Variables.Globals;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class signal_quality {
	private int[] buffer;
	private int buffer_loc;
	private int len = 10*100;
	private feature_detect ft;
	public boolean isOK;
	public signal_quality()
	{
		ft= new feature_detect();
		reset();
	}
	public void reset()
	{
		buffer=new int[len];
		buffer_loc=0;
	}
	public void add_val(int val)
	{
		buffer[buffer_loc] = val;
        buffer_loc++;
        if (buffer_loc == len) {
            buffer_loc = 0;

            process_signal ps = new process_signal(Arrays.copyOf(buffer, len));
            Thread t = new Thread(ps);
            t.run();
        }
	}
	
	private class process_signal implements Runnable{
		private int[] proc_int;
		private List<Beat> beats;
		public process_signal(int[] arr) {
			// TODO Auto-generated constructor stub
			proc_int=arr;
			beats=new LinkedList<>();
			ft.reset();
			
			
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			beats.clear();
			ft.fitBeats(proc_int);
			boolean isGood=true;
			int[] sys_points=ft.get_SysPeakPoints();
			double[] rr = new double[sys_points.length-1];
			double min=50;
			double max=0;
			for(int i=0;i<rr.length;i++)
	        {
				
	            rr[i]=(sys_points[i+1]-sys_points[i]+0.0)/100.0;
	            if(rr[i]>3)
	            {
	            	isGood=false;
	            	break;
	            }
	            if(rr[i]>max)
	            {
	            	max=rr[i];
	            }
	            if(rr[i]<min)
	            {
	            	min=rr[i];
	            }
	            
	        }

			isGood=isGood&&(max/min<2.2);


			if(!isGood)
			{
				Globals.driverStateManager.reset_recording();
			}

		}
		
		
		
	}
}
