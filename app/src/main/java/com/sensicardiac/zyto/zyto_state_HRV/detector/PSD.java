package com.sensicardiac.zyto.zyto_state_HRV.detector;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

public class PSD {
	
	private double[] freqs;
	private double[] psd;
	
	public PSD(){
		
	}
	public void test()
	{
		int N=60;
		double[] time = new double[N];
		double[] cosf = new double[N];
		double x=0;
		for(int i=0;i<N;i++)
		{
			
			time[i]=x;
			double cos1=Math.cos(2*Math.PI*x*0.05);
			double cos2=Math.cos(2*Math.PI*x*0.1);
			double cos3=Math.cos(2*Math.PI*x*0.15);
			double cos4=Math.cos(2*Math.PI*x*0.4);
			x=x+ (Math.random()-1)/4+1;
			cosf[i]=cos1+cos2+cos3+cos4;
		}
		psd_calculation(time, cosf, 0.02, 0.5, 500);
		
	}
	//Method that fits one sinusoid to the data.
	//Source: http://www.sal.ufl.edu/eel6537_2010/LSP.pdf
	public double least_squares_fit(double[] x,double[] y,double omega)
	{
		RealMatrix Y=MatrixUtils.createColumnRealMatrix(y);
		int N=x.length;
		RealMatrix A=MatrixUtils.createRealMatrix(N, 2);
		for(int i=0;i<N;i++)
		{
			A.setEntry(i, 0,Math.cos(omega*x[i]) );
			A.setEntry(i, 1,Math.sin(omega*x[i]) );
		}
		RealMatrix R =A.transpose().multiply(A);
		RealMatrix r =A.transpose().multiply(Y);
		
		RealMatrix Pls=(r.transpose().multiply(MatrixUtils.inverse(R)).multiply(r));
		
		
		
		return Pls.getEntry(0, 0)/(N+0.0);
	}
	//Perform multiple PSDs over the data
	public void psd_calculation(double x[],double y[],double start,double end,int N)
	{
		double[] freqs=new double[N];
		double[] psd=new double[N];
		double rad=0;
		double diff=(end-start)/N;
		for(int i=0;i<N;i++)
		{
			freqs[i]=start+i*diff;
			rad=2*Math.PI*freqs[i];
			psd[i]=least_squares_fit(x, y, rad);
		}
		this.freqs=freqs;
		this.psd=psd;
	}
	public double[] getFreqs() {
		return freqs;
	}
	public double[] getPsd() {
		return psd;
	}

	public double integrate(double start,double stop)
	{
		double out=0;
		//find the start and end indexes
		int start_ind=Math.max(0, (int)Math.floor(start*100));
		int end_ind=Math.min(freqs.length-1, (int)Math.floor(stop*100));;
		
		for(int i=start_ind;i<end_ind-1;i++)
		{
			double x1=freqs[i];
			double x2=freqs[i+1];
			double y1=psd[i];
			double y2=psd[i+1];
			out+=(x2-x1)*((y1+y2)/2);
		}
		
		return out;
	}
}
