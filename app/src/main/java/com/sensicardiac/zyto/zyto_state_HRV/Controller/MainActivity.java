package com.sensicardiac.zyto.zyto_state_HRV.Controller;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;


import com.sensicardiac.afiledialog.FileChooserActivity;
import com.sensicardiac.afiledialog.FileChooserDialog;

import com.sensicardiac.zyto.zyto_state_HRV.SQL_DB.DB_COMMANDS;
import com.sensicardiac.zyto.zyto_state_HRV.SQL_DB.DataLoader;
import com.sensicardiac.zyto.zyto_state_HRV.SQL_DB.HR_RECORDING;
import com.sensicardiac.zyto.zyto_state_HRV.SQL_DB.LOCAL_SQL_DB_Handler;
import com.sensicardiac.zyto.zyto_state_HRV.States.Bluetooth_on_state;
import com.sensicardiac.zyto.zyto_state_HRV.Variables.Const;
import com.sensicardiac.zyto.zyto_state_HRV.Parser.DataParser;
import com.sensicardiac.zyto.zyto_state_HRV.States.Device_connected_state;
import com.sensicardiac.zyto.zyto_state_HRV.Variables.Globals;
import com.sensicardiac.zyto.zyto_state_HRV.Parser.PackageParser;
import com.sensicardiac.zyto.zyto_state_HRV.R;
import com.sensicardiac.zyto.zyto_state_HRV.States.streaming_state;

import org.json.simple.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;


import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;


public class MainActivity extends Activity implements PackageParser.OnDataChangeListener {

    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    public static Context mContext;
    private DataParser mDataParser;
    private PackageParser mPackageParser;
    private BluetoothAdapter mBluetoothAdapter;
    public WebView mywebview;
    private BluetoothLeService mBluetoothLeService;
    private boolean mIsNotified;
    private boolean mIsScanFinished;
    private BluetoothDevice currentDevice;
    private Set<BluetoothDevice> deviceList;
    private BluetoothDevice[] bluetoothDevicesArr;
    private BluetoothGattCharacteristic chReceive;
    private BluetoothGattCharacteristic chChangeBtName;
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkForUpdates();
        setupBluetooth();
        setmContext(getApplicationContext());
        Globals.init(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("This app needs location access");
                builder.setMessage("Please grant location access so this app can detect beacons.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                        }
                    }
                });
                builder.show();
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("This Write Access to Stordage Device");
                builder.setMessage("Please grant Stroage access so this app can write files to storage Media.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL_STORAGE);
                        }
                    }
                });
                builder.show();
            }
        }
        mywebview = (WebView) findViewById(R.id.webview);
        if (savedInstanceState != null)
            mywebview.restoreState(savedInstanceState);
        else {
            mywebview.loadUrl("file:///android_asset/login.html");
//            mywebview.loadUrl("file:///android_asset/index.html");
        }



//        mywebview.loadUrl("file:///android_asset/login.html");
//        mywebview.loadUrl("www.google.com");
        mywebview.getSettings().setJavaScriptEnabled(true);
        mywebview.getSettings().setDomStorageEnabled(true);
        deviceList = new HashSet<>();
        mDataParser = new DataParser(DataParser.Protocol.BCI, new DataParser.onPackageReceivedListener() {
            @Override
            public void onPackageReceived(int[] dat) {

//                Log.i("Stream", "onPackageReceived: " + Arrays.toString(dat));
                if (mPackageParser == null) {
                    mPackageParser = new PackageParser(MainActivity.this);
                }

                mPackageParser.parse(dat);
            }
        });

        mDataParser.start();
        mywebview.addJavascriptInterface(new MyReceiver(), "receiver");
        mywebview.addJavascriptInterface(new  MessageReceiver(), "receiver1");
        this.registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
        DataLoader dl = new DataLoader();
        mywebview.addJavascriptInterface(dl,"db_count");
    }



    @Override
    protected void onResume() {
        super.onResume();
        checkForCrashes();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(currentDevice.getAddress());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterManagers();
        unregisterReceiver(mGattUpdateReceiver);
    }
    //TODO David to fix onDestroy Item on Orientation Change this is where the problem comes in and no service can be removed.
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterManagers();
        mDataParser.stop();
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }

    public void Destroy(){
        mDataParser.stop();
        unbindService(mServiceConnection);
        mBluetoothLeService = null;

    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    String test = mywebview.getUrl();
                    if (test.contains("file:///android_asset/settings.html")){
                        mywebview.loadUrl("file:///android_asset/login.html");
                    }
                    else if (mywebview.canGoBack()) {
                        mywebview.goBack();
                    } else {
//                        Destroy();
                        finish();
                        System.exit(0);
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        mywebview.saveState(outState);
    }

    public void notifyStart() {
        if (chReceive != null) {
            if (!mIsNotified) {
                mBluetoothLeService.setCharacteristicNotification(chReceive, true);
                Globals.driverStateManager.setState(new streaming_state());
                Log.i("Stream", ">>>>>>>>>>>>>>>>>>>>START<<<<<<<<<<<<<<<<<<<");
            }

            mIsNotified = !mIsNotified;
        }
    }

    public void notifyStop() {
        if (chReceive != null) {
            if (mIsNotified)
            {

                mBluetoothLeService.setCharacteristicNotification(chReceive, false);
                Globals.driverStateManager.setState(new Device_connected_state());
                Log.i("Stream", ">>>>>>>>>>>>>>>>>>>>STOP<<<<<<<<<<<<<<<<<<<");
            }
            mIsNotified = !mIsNotified;
        }
    }


    @Override
    public void onSpO2ParamsChanged() {
        PackageParser.OxiParams params = mPackageParser.getOxiParams();
        Globals.webSocket_server.sendOxParams(params);
        if (params.getPulseRate() == 255 & params.getSpo2() == 127) {
            notifyStop();
        }
        mHandler.obtainMessage(Const.MESSAGE_OXIMETER_PARAMS, params.getSpo2(), params.getPulseRate()).sendToTarget();
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {

            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    if (!mIsScanFinished) {
//
                        Globals.webSocket_server.sendError("Timeout: can't connect to device");
                        mIsNotified = true;
                    }
                }
            }, 3000);

            mBluetoothAdapter.startLeScan(mLeScanCallback);
            mIsScanFinished = false;
        } else {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
            mIsScanFinished = true;
        }
    }
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case Const.MESSAGE_OXIMETER_PARAMS:

                    break;
                case Const.MESSAGE_OXIMETER_WAVE:

                    break;
            }
        }
    };
    private void setupBluetooth() {
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
    }
    public boolean isBluetoothEnabled()
    {
        return mBluetoothAdapter.isEnabled();
    }

    public void turnOnBluetooth() {
        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();

        }

    }

    public void turn_off_bluetooth() {
        if (mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.disable();

        }
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            // It means the user has changed his bluetooth state.
            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {

                if (mBluetoothAdapter.getState() == BluetoothAdapter.STATE_OFF) {
                    // The user bluetooth is turning off yet, but it is not disabled yet.
                    Globals.driverStateManager.stop_bluetooth();
                }

                if (mBluetoothAdapter.getState() == BluetoothAdapter.STATE_ON) {
                    // The user bluetooth is already disabled.
                    Globals.driverStateManager.setState(new Bluetooth_on_state());
                }

            }
        }
    };

    public boolean selectDevice(int dev_num) {
        if(dev_num<bluetoothDevicesArr.length) {
            currentDevice = bluetoothDevicesArr[dev_num];
            mIsScanFinished = false;
            mIsNotified = false;
            scanLeDevice(true);
            return true;
        }
        else
        {
            return false;
        }
//        Log.d("Bluetooth_connect","The selected device is"+currentDevice.getName());
//        Intent gattServiceIntent = new Intent(MainActivity.this, BluetoothLeService.class);
//
//        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
//        Log.d("Bluetooth_connect","Service binded");


    }




    public void scanLeDevice_find(final boolean enable) {
        deviceList.clear();

        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mBluetoothAdapter.stopLeScan(mLeScanCallback_find);
                    List<String> names = new LinkedList<>();
                    List<String> macs = new LinkedList<>();


                    bluetoothDevicesArr = deviceList.toArray(new BluetoothDevice[deviceList.size()]);
                    for (int i = 0; i < bluetoothDevicesArr.length; i++) {
                        names.add(bluetoothDevicesArr[i].getName());
                        System.out.println(bluetoothDevicesArr[i].getName());
                        macs.add(bluetoothDevicesArr[i].getAddress());
                    }
                    JSONObject obj = new JSONObject();
                    obj.put("command", "device_list");

                    obj.put("dev_names", names);
                    obj.put("macs", macs);

                    Globals.webSocket_server.sendText(obj.toJSONString());
//                    scanLeDevice_find(false);


                }
            }, 3000);

            mBluetoothAdapter.startLeScan(mLeScanCallback_find);
            mIsScanFinished = false;
        } else {
            mBluetoothAdapter.stopLeScan(mLeScanCallback_find);
            mIsScanFinished = true;
        }
    }

    // Device scan callback.

    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (device.getName()!=null){
                                if (device.getName().equals(currentDevice.getName())) {


                                    scanLeDevice(false);
//                                tvStatusBar.setText("Name:"+device.getName()+"     "+"Mac:"+device.getAddress());

                                    //start BluetoothLeService

                                    Intent gattServiceIntent = new Intent(MainActivity.this, BluetoothLeService.class);


                                    try{

                                        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
                                    }
                                    catch (Exception e){
                                        Globals.webSocket_server.sendError("Couldn't bind service connection");
                                     //   Log.e("GAT","Error : "+e);
                                    }

                                }
                            }}
                    });
                }
            };

    private BluetoothAdapter.LeScanCallback mLeScanCallback_find =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!device.equals(null) && device.getName() != null) {
                                deviceList.add(device);
//
                            }
                        }
                    });
                }
            };

    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {


            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Globals.webSocket_server.sendError("Can't initialise bluetooth");
                finish();

            }
            // Automatically connects to the device upon successful start-up initialization.

            mBluetoothLeService.connect(currentDevice.getAddress());

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

            Globals.driverStateManager.setState(new Device_connected_state());
        }
    };

    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {

//                Toast.makeText(MainActivity.this, "Connected",Toast.LENGTH_SHORT).show();
                Globals.driverStateManager.setState(new Device_connected_state());
                mIsNotified = false;
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                cleanup_connection();
//                Toast.makeText(MainActivity.this, "Disconnected",Toast.LENGTH_SHORT).show();
                Globals.driverStateManager.setState(new Bluetooth_on_state());

            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                initCharacteristic();
//                rlInfoBtns.setVisibility(View.VISIBLE);

            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                Toast.makeText(MainActivity.this,
                        intent.getStringExtra(BluetoothLeService.EXTRA_DATA),
                        Toast.LENGTH_SHORT).show();
            } else if (BluetoothLeService.ACTION_SPO2_DATA_AVAILABLE.equals(action)) {
                mDataParser.add(intent.getByteArrayExtra(BluetoothLeService.EXTRA_DATA));
            }
        }
    };
    public void cleanup_connection()
    {


    //    mBluetoothLeService.disconnect();
    //    mBluetoothLeService.close();

        unbindService(mServiceConnection);
        mBluetoothLeService.disconnect();
        mBluetoothLeService.close();


        mBluetoothLeService = null;
//                rlInfoBtns.setVisibility(View.GONE);
        mIsNotified = false;
        deviceList.clear();
        bluetoothDevicesArr=null;
        currentDevice=null;
        unregisterReceiver(mGattUpdateReceiver);

        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
    }
    public void initCharacteristic() {

        List<BluetoothGattService> services =
                mBluetoothLeService.getSupportedGattServices();
        BluetoothGattService mInfoService = null;
        BluetoothGattService mDataService = null;
        for (BluetoothGattService service : services) {
            if (service.getUuid().equals(Const.UUID_SERVICE_DATA)) {
                mDataService = service;

            }
        }
        if (mDataService != null) {
            List<BluetoothGattCharacteristic> characteristics =
                    mDataService.getCharacteristics();
            for (BluetoothGattCharacteristic ch : characteristics) {
                if (ch.getUuid().equals(Const.UUID_CHARACTER_RECEIVE)) {

                    chReceive = ch;
//                    Log.d("characteristics",chReceive.getUuid().toString());
                } else if (ch.getUuid().equals(Const.UUID_MODIFY_BT_NAME)) {
                    chChangeBtName = ch;
//                    llModifyBtName.setVisibility(View.VISIBLE);
                }
            }
        }
    }
    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothLeService.ACTION_SPO2_DATA_AVAILABLE);
        return intentFilter;
    }

    @Override
    public void onSpO2WaveChanged(int wave, int pulse, int spo2, int pi) {
        Globals.webSocket_server.sendWavParams(wave, pulse, spo2, pi);
        Globals.ft.add_val(wave);
        Globals.sq.add_val(wave);
        Globals.add_to_buffer(wave);

        mHandler.obtainMessage(Const.MESSAGE_OXIMETER_WAVE, wave, 0).sendToTarget();
    }

    private void checkForCrashes() {
        CrashManager.register(this);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }


    public class MyReceiver{
        @JavascriptInterface
        public void receive(String[] input,String name){
            Log.d("JS_ARRAY","AR :" + Arrays.toString(input));
            Log.d("JS_ARRAY","NAME :" + name);
            SimpleDateFormat simpleDate =  new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
            Date now = new Date();
            Log.d("Save file","Length: "+input.length);
            String timestamp = simpleDate.format(now);
            Log.d("time",timestamp);
            int REQUEST_WRITE_EXTERNAL_STORAGE=1;
            File Folder = new File (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) +File.separator +"Zyto/");
            if (!Folder.exists()){
                Folder.mkdirs();
            }
            try {
                File myFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) +File.separator +"Zyto/" + name+"_" + timestamp + ".csv");
                myFile.createNewFile();
                FileOutputStream fOut = new FileOutputStream(myFile);
                OutputStreamWriter myOutWriter =
                        new OutputStreamWriter(fOut);
                String inputs = Arrays.toString(input);

                myOutWriter.append(inputs.substring(1,inputs.length()-1));
                myOutWriter.close();
                fOut.close();
                Toast.makeText(getBaseContext(),
                        "Done writing file " + myFile.toString(),
                        Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Toast.makeText(getBaseContext(), e.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }

            // String FileName = name + DateFormat.getDateTimeInstance().format(new Date());
//            File Folder = new File ("/sdcard/Zyto/");
//            if (!Folder.exists()){
//                Folder.mkdirs();
//            }


            //  File file = new File ( "sdcard/Zyto/Edward.txt");
//            File file = new File("/sdcard/Zyto/mysdfile.txt");

//            if (!file.exists()){
//                try {
//                    file.createNewFile();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }else if (file.exists()){
//                file.delete();
//                try {
//                    file.createNewFile();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            try {
//                FileOutputStream outputStream = openFileOutput(String.valueOf(file),Context.MODE_APPEND);
//                try{
//                    for (String s : input){
//                        outputStream.write(s.getBytes());
//                    }
//                    outputStream.close();
//
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            }
//
//        }

        }

    }

    public class MessageReceiver{
        @JavascriptInterface
        public void receive1(String data){
            switch (data){
                case "bluetooth_on":
//                    Toast.makeText(getBaseContext(),
//                            "Bluetooth is On",
//                            Toast.LENGTH_LONG).show();
                    break;
                case "bluetooth_off":
                    Toast.makeText(getBaseContext(),
                            "Bluetooth is off. If it does not restart please turn it on in the Settings Menu.",
                            Toast.LENGTH_LONG).show();
                    break;
                case "device_list":
                    Toast.makeText(getBaseContext(),
                            "Could not detect the device, please make sure it is turned on.",
                            Toast.LENGTH_LONG).show();
                    break;
                case "device_connected":
                    Toast.makeText(getBaseContext(),
                            "Device is connected.",
                            Toast.LENGTH_LONG).show();
                    break;
                case "streaming_state_start":
                    Toast.makeText(getBaseContext(),
                            "Data stream started.",
                            Toast.LENGTH_LONG).show();
                    break;
                case "streaming_state_stop":
                    Toast.makeText(getBaseContext(),
                            "Data stream stopped due to 'no data error'.",
                            Toast.LENGTH_LONG).show();
                    break;
                case "Timeout":
                    Toast.makeText(getBaseContext(),
                            "Something went wrong, please scan again.",
                            Toast.LENGTH_LONG).show();
                    break;
                case "Sendmail":
                    Open_File();
                    break;
                case "connect_device_first":
                    Toast.makeText(getBaseContext(),
                            "Please connect a device before attempting to make a recording.",
                            Toast.LENGTH_LONG).show();
                    break;
                default:
                    DB_COMMANDS._signal = data;
            }




        }

//        public void receive2(String data){
//            initCharacteristic();
//            Destroy();
//        }

    }

    public void Open_File(){
        FileChooserDialog dialog = new FileChooserDialog(this);
        dialog.setContext(this.getApplicationContext());
        // Assign listener for the select event.
        dialog.addListener(this.onFileSelectedListener);
        File Folder = new File (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) +File.separator +"Zyto/");
        dialog.setShowCancelButton(true);
        dialog.setShowOKButton(true);

        //dialog.setShowConfirmation(true,true);

        dialog.loadFolder(Folder.getPath().toString());
        // Show the dialog.
        dialog.show();

    }

    private FileChooserDialog.OnFileSelectedListener onFileSelectedListener = new FileChooserDialog.OnFileSelectedListener() {
        public void onFileSelected(Dialog source, File file) {
           // source.hide();
            Toast toast = Toast.makeText(getApplicationContext(), "File selected: " + file.getName(), Toast.LENGTH_LONG);
            toast.show();
        }
        public void onFileSelected(Dialog source, File folder, String name) {
         //   source.hide();
            Toast toast = Toast.makeText(getApplicationContext(), "File created: " + folder.getName() + "/" + name, Toast.LENGTH_LONG);
            toast.show();
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            boolean fileCreated = false;
            String filePath = "";

            Bundle bundle = data.getExtras();
            if(bundle != null)
            {
                if(bundle.containsKey(FileChooserActivity.OUTPUT_NEW_FILE_NAME)) {
                    fileCreated = true;
                    File folder = (File) bundle.get(FileChooserActivity.OUTPUT_FILE_OBJECT);
                    String name = bundle.getString(FileChooserActivity.OUTPUT_NEW_FILE_NAME);
                    filePath = folder.getAbsolutePath() + "/" + name;
                } else {
                    fileCreated = false;
                    File file = (File) bundle.get(FileChooserActivity.OUTPUT_FILE_OBJECT);
                    filePath = file.getAbsolutePath();
                }
            }

            String message = fileCreated? "File created" : "File opened";
            message += ": " + filePath;
            Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public static Context getmContext() {
        return mContext;
    }

    public static void setmContext(Context context) {
        mContext = context;
    }


}
