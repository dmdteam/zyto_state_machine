package com.sensicardiac.zyto.zyto_state_HRV.SQL_DB;

import android.webkit.JavascriptInterface;

/**
 * Created by Edward on 2017/03/06.
 */

public class DataLoader {
    @JavascriptInterface
    public String get_Record_Count(){

        String data = DB_COMMANDS.get_database_size().toJSONString();

        return data;
    }

    @JavascriptInterface
    public String get_one_Record(int num){
        String data = DB_COMMANDS.get_one_recording(num).toJSONString();
        return data;
    }



}
