package com.sensicardiac.zyto.zyto_state_HRV.States;

import android.util.Log;

public class DriverStateManager implements IDriverState {

	private DriverState CurrentState=null;
	public DriverState getCurrentState()
	{
		return CurrentState;
	}
	public DriverStateManager()
	{
		setState(new Unconnected_state());
	}
	public void setState(DriverState driverState)
	{
		if(driverState.equals(CurrentState))
		{
			Log.d("STATE","Ignoring Changing to state: "+driverState.getStateName()+ "\n");
			return;
		}
		try {
			CurrentState.onExit();
		}catch(Exception e)
		{
			System.out.println(e.toString());
		}
		Log.d("STATE","Changing to state: "+driverState.getStateName());
		CurrentState=driverState;
		CurrentState.send_confirm();
		CurrentState.onEntry();
	}
	public void start_stream() {
		// TODO Auto-generated method stub
		CurrentState.start_stream();
		
	}

	public void stop_stream() {
		// TODO Auto-generated method stub
		CurrentState.stop_stream();
	}

	public void start_bluetooth() {
		// TODO Auto-generated method stub
		CurrentState.start_bluetooth();
	}

	public void stop_bluetooth() {
		// TODO Auto-generated method stub
		CurrentState.stop_bluetooth();
	}

	public void get_devices() {
		// TODO Auto-generated method stub
		CurrentState.get_devices();
	}

	public void send_dev_num(int num) {
		// TODO Auto-generated method stub
		CurrentState.send_dev_num(num);
		
	}
	public void recieve_message(String text) {
		// TODO Auto-generated method stub
		CurrentState.recieve_message(text);
	}
	public void chooseBluetoothState()
	{
		CurrentState.chooseBluetoothState();
	}
	public void send_confirm() {
		// TODO Auto-generated method stub
		CurrentState.send_confirm();
	}
	public void web_socketClose() {
		// TODO Auto-generated method stub
		CurrentState.web_socketClose();
		
	}
	@Override
	public void get_state() {
		// TODO Auto-generated method stub
		CurrentState.get_state();
		
	}

	@Override
	public void send_data(String text) {
		CurrentState.send_data(text);
	}

	@Override
	public void reset_recording()
	{
		CurrentState.reset_recording();
	}





}
