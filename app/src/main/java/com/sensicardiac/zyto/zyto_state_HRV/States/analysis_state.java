package com.sensicardiac.zyto.zyto_state_HRV.States;

import android.os.AsyncTask;
import android.util.Log;

import com.sensicardiac.zyto.zyto_state_HRV.SQL_DB.DB_COMMANDS;
import com.sensicardiac.zyto.zyto_state_HRV.Variables.Globals;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by davidfourie on 2017/02/21.
 */

public class analysis_state extends DriverState {

    BackgroundWorker backgroundWorker;
    @Override
    public void recieve_message(String text) {
        Object obj = null;

        try {
            obj = Globals.parser.parse(text);
            JSONObject jsonObject = (JSONObject) obj;
            String command=(String)jsonObject.get("command");
            switch(command)
            {




                case "get_state":
                    get_state();
                    break;

                default:
                    Globals.webSocket_server.sendError("Not a valid command for state");
                    break;


            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getStateName() {
        return "analysis_state";
    }

    @Override
    public void onEntry() {

        Globals.save_file("");
    backgroundWorker=new BackgroundWorker();
        backgroundWorker.execute();


    }

    @Override
    public void onExit() {

    }

    @Override
    public void stop_bluetooth() {
        Globals.mainActivity.cleanup_connection();


        Globals.driverStateManager.setState(new Bluetooth_off_state());
    }

    @Override
    public void web_socketClose() {

        Globals.driverStateManager.setState(new Unconnected_state());

    }

    private class BackgroundWorker extends AsyncTask<Void, Void, double[]>
    {

        @Override
        protected double[] doInBackground(Void... voids) {

            //Start processing
            Globals.ft.fitBeats(Globals.recorded_file);
            int[] peaks=Globals.ft.get_SysPeakPoints();
            double[] params=Globals.ft.getHR_params();

            return params;
        }

        /**
         * Paramaters for DB Saving
         * @param  params LF_HF=params[0], RMSSD=params[1], pnn20=params[2], pnn50=params[3],pnn70=params[4]
         */
        @Override
        protected void onPostExecute(double[] params)
        {
            JSONObject jso = new JSONObject();
            jso.put("command","set_HRV_params");

            List<Double> out= new LinkedList<>();
            for(int i=0;i<params.length;i++)
            {
                out.add(params[i]);

            }
            jso.put("params",out);
            Log.d("Params","1: PARAMS : " + params[1]);
            Log.d("Params","2: JSO" + jso);
            Globals.webSocket_server.sendText(jso.toJSONString());
            Globals.driverStateManager.setState(new streaming_state());
            DB_COMMANDS._HF_LF = String.valueOf(params[0]);
            DB_COMMANDS._RMSSD = String.valueOf(params[1]);
            DB_COMMANDS.upload_new();
        }


    }
}
