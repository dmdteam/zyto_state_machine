var state = "unconnected";
var dev_list = [];
var wav_counter = 0;
var wav_value = 0;
var prev_wav_val = 0;
var stream_count = 0;
 var obj;
 var jsonCommand
 var ws;
 var  running_state;
 var after_crash = false;
            var json = [{"cool":"34.33"},{"alsocool":"45454"}];
			function start_stream() {
                sessionStorage.setItem('stoping', false);

			    obj = new Object();
			    obj.command = "start_stream";

			pos=0;
			    jsonCommand = JSON.stringify(obj);

			    ws.send(jsonCommand);
			    stream_start_btn.disabled = true;
			    start_bluetooth_btn.disabled = true;
			    stream_stop_btn.disabled = false;
			//    processing_reset();


			}
			function stop_stream() {
			    obj = new Object();
			    obj.command = "stop_stream";

                sessionStorage.setItem('stoping', true);
			    jsonCommand = JSON.stringify(obj);

			    this.ws.send(jsonCommand);

			    pos = 0;
			    stream_start_btn.disabled = false;
			    start_bluetooth_btn.disabled = false;
			    stream_stop_btn.disabled = true;

			}

			function start_bluetooth()
			{
			    obj = new Object();
			    obj.command = "start_bluetooth";


			    jsonCommand = JSON.stringify(obj);

			    ws.send(jsonCommand);

			}

			function stop_bluetooth()
			{
			    obj = new Object();
			    obj.command = "stop_bluetooth";


			    jsonCommand = JSON.stringify(obj);

			    ws.send(jsonCommand);

			}

			function start_recording()
            {
                obj = new Object();
                obj.command = "start_recording";


                jsonCommand = JSON.stringify(obj);

                ws.send(jsonCommand);

            }

            function stop_recording()
            {
           	obj = new Object();
            obj.command = "stop_recording";


            jsonCommand = JSON.stringify(obj);

            ws.send(jsonCommand);

            }

            function start_analysis()
            {
                obj = new Object();
                obj.command = "start_analysis";
                jsonCommand = JSON.stringify(obj);

                ws.send(jsonCommand);
            }







			function get_devices() {
				//$('#devices').empty();
            //    modal_loader.style.visibility = 'visible';
			    obj = new Object();
			    obj.command = "get_devices";


			    jsonCommand = JSON.stringify(obj);

			    ws.send(jsonCommand);

			    pos = 0;

			}
			function send_dev_number()
			{
			  //  modal_loader.style.visibility = 'visible';
				var dev_num = dev_num_text.value;
				if (dev_num !== null)
				{

				    var obj = new Object();
				    obj.command = "send_dev_num";
				    obj.dev_num = dev_num.toString();
				    var jsonCommand = JSON.stringify(obj);

				    ws.send(jsonCommand);
				}
			}

			function set_recording_time()
            {


                var dev_num = dev_time_text.value;
                if (dev_num !== null)
                {

                var obj = new Object();
                obj.command = "recording_time";
                obj.time = dev_num.toString();
                var jsonCommand = JSON.stringify(obj);

                ws.send(jsonCommand);
                }
            }
			function check_state()
			{

			    obj = new Object();
			    obj.command = "get_state";


			    jsonCommand = JSON.stringify(obj);

			    ws.send(jsonCommand);

			}
			function printText(s)
			{
				inc.value=s+"\n"+inc.value;
			}
			function parseMessage(s)
			{

			    var parse_data = JSON.parse(s);
			    switch (parse_data.command)
			    {
				case "state_val":
				    server_state = parse_data.state;
				    printText("State: "+server_state);
				    console.log("State: "+server_state);
				    sessionStorage.setItem('State',server_state);
				    switch(server_state)
				    {
				    case "bluetooth_on":
                        if (after_crash === true){
                            check_state();
                            after_crash = false;
                        }
				    	start_bluetooth_btn.disabled = true;
					    stop_bluetooth_btn.disabled = false;
					    get_devices_btn.disabled=false;
					    stream_start_btn.disabled=true;
					     sessionStorage.setItem('BT','Bluetooth Connected');
                         sessionStorage.setItem('BT_con','BT_con_True');
                         var t = "bluetooth_on"
                         receiver1.receive1(t);
				    	break;
				    case "bluetooth_off":
				    	start_bluetooth_btn.disabled = false;
					    stop_bluetooth_btn.disabled = true;
					    get_devices_btn.disabled=false;
					    stream_start_btn.disabled=true;
					     sessionStorage.setItem('BT','Bluetooth Disconnected');
                        var t = "bluetooth_off"
                        receiver1.receive1(t);
				    	break;

				    case "device_connected":

                    	get_devices_btn.disabled=false;
                        stream_start_btn.disabled=false;

                        dev_time_text.disabled=false;
                        record_time_btn.disabled=false;
                        analyse_btn.disabled=false;
                        var t = "device_connected"
                        receiver1.receive1(t);
                            break;

				    case "streaming_state":
				     running_state = true;
				    var t = "streaming_state_start"
				   // modal_loader.style.visibility = 'hidden';
                    receiver1.receive1(t);
                        stream_start_btn.disabled=true;
                        stream_stop_btn.disabled=false;
                        record_start_btn.disabled=false;
                        record_stop_btn.disabled=true;

				    break;


				    case "recording_state":
                        record_start_btn.disabled=true;
                        record_stop_btn.disabled=false;
                    	break;



				}
				break;


				case "error":
//				            var t = parse_data.message;
//				            var timeout = "Timeout";
//				            if (t.indexOf("Timeout") >= 0){
//				            var s = "Timeout";
//                            receiver1.receive1(s);}
					printText("Error: "+parse_data.message);
				break;

				case "confirm":

				    break;


				case "sig_data":
				    //value=low_pass.add_val(parse_data.value);
				    //value=high_pass.add_val(value);
				    value = parse_data.value;
//				    chart.options.data[0].dataPoints[pos].y = value;
//				    pos++;
//
//				    if (pos == 50) {
//					pos = 0;
//				    }
				    break;
				case "device_list":

                    var dev_sel = document.getElementById("devices");
                 //   dev_sel.innerHTML = "";
				    names=parse_data.dev_names;
				    macs=parse_data.macs;
				    connect_device_btn.disabled=false;
				    connect_device_btn.style.visibility = 'visible';
				    dev_num_text.value=0;
                   console.log(names.length);
				    if(names.length>0)
				    {
					    dev_num_text.disabled=false;
					    for(var i =names.length-1;i>=0;i--)
                        {
                            dev_list[i] = names[i];
                            printText("Number: "+i+" Name: "+names[i]+" Mac address: "+macs[i]);
                            console.log("Number: "+i+" Name: "+names[i]);
    //					    inc.value
    //					    sessionStorage.setItem('dev_list',dev_list);
    //					    var dev_sel = document.getElementById("devices");
    					     var option = document.createElement("option");
                               option.text = dev_list[i];
                               option.value = i;
                               dev_sel.appendChild(option);

    //					    console.log("Name: "+names[i]+ " Mac address: "+macs[i]);
                        }
                 //   modal_loader.style.visibility = 'hidden';
				    }
				    else if(names.length<=0)
				    {
				        var t = "device_list"
                        receiver1.receive1(t);
                        check_state();
                //        connect_device_btn.style.visibility = 'hidden';
					    connect_device_btn.disabled=true;
					//    modal_loader.style.visibility = 'hidden';
				    }

				    break;
			    case "set_oxi_param":
				    spo2=parse_data.spo2;
				    pulse=parse_data.pulse;
				    pi=parse_data.pi;

				     spo2_text.value="SPO2: "+spo2;
					pulse_text.value="Pulse rate: "+pulse;
					pulse_ind_text.value="Pulse index: "+pi;
				break;
			 case "set_wave_param":
				 wave=parse_data.wav;
				    spo2=parse_data.spo2;
				    pulse=parse_data.pulse;
				    pi=parse_data.pi;
				 //



// Save the wave file

//
//				  console.log("new array size:" +a.length)


				    wav_value = wave;

				//  chart.options.data[0].dataPoints[pos].y = new_y;
				  pos++;
				  if(pos==100*5)
				  {
					  pos=0;
				  }
				break;

             case "set_HRV_params":

                params=parse_data.params;
                LF_HF=params[0];
                RMSSD=params[1];
                pnn20=params[2];
                pnn50=params[3];
                pnn70=params[4];
                printText("LF/HF ratio is: "+LF_HF);
                printText("RMSSD is: "+RMSSD);
                printText("pnn20 is: "+pnn20 );
                printText("pnn50 is: "+pnn50 );
                printText("pnn70 is: "+pnn70 );

             break;



			 case "set_signal_param":
			        var beat_obj={
			        p0:parse_data.p0,
			        p1:parse_data.p1,
			        p2:parse_data.p2,
			        p3:parse_data.p3,
			        p4:parse_data.p4};

			        beats = JSON.parse(sessionStorage.getItem('beats'));
                    beats.push(beat_obj);
                    sessionStorage.setItem('beats',JSON.stringify(beats));
			 break;
			}
}
var beats = [];
			function start_p()
			{
				console.log("starting log");
			    wsImpl = window.WebSocket || window.MozWebSocket;
			    window.ws = new wsImpl('ws://localhost:8887/');
			    inc = document.getElementById("output_area");

			    get_state_btn = document.getElementById("get_state_btn_id");
			    start_bluetooth_btn = document.getElementById("start_bluetooth_btn_id");
			    stop_bluetooth_btn = document.getElementById("stop_bluetooth_btn_id");
			    get_devices_btn = document.getElementById("get_devices_btn_id");
			    connect_device_btn=document.getElementById("connect_device_btn_id");
			    dev_num_text=document.getElementById("dev_num_text_id");
			    stream_start_btn=document.getElementById("stream_start_btn_id")
			    stream_stop_btn=document.getElementById("stream_stop_btn_id")
			    spo2_text=document.getElementById("spo2_text_id");
			    pulse_text=document.getElementById("pulse_text_id");
			    pulse_ind_text=document.getElementById("pulse_ind_text_id");

			    dev_time_text=document.getElementById("dev_time_text_id");
			    record_time_btn=document.getElementById("record_time_btn_id");


                record_start_btn=document.getElementById("record_start_btn_id");
                record_stop_btn=document.getElementById("record_stop_btn_id");

                analyse_btn=document.getElementById("analyse_btn_id");


			    modal_loader = document.getElementById("modal_loader");
		//	    processing_setup(100);
                beats = [];
                sessionStorage.setItem('beats', JSON.stringify(beats));

                var save_arr = [];

                sessionStorage.setItem('save_arr', JSON.stringify(save_arr));


			    pos = 0;
			    console.log("starting log");
			    var dataPoints = [];

			    for (i = 0; i < 100*5; i++)
			    {
				dataPoints.push({y: 0});

			    }



//			    chart = new CanvasJS.Chart("chartContainer5", {
//				title: {
//				    text: "Linechart"
//				},
//				interactivityEnabled: false,
//				animationEnabled: true,
//				exportEnabled: true,
//				 responsive:true,
//				maintainAspectRatio: true,
//
//				type: "line",
//				data: [{
//					type: "line",
//					dataPoints: dataPoints
//				    }]
//			    });
//			    chart.render();
//			    setInterval(function () {
//					chart.render();
//				}, 50);


			    window.ws.onmessage = function (evt) {

				parseMessage(evt.data);

			    };

			    window.ws.onopen = function () {
				if (state !== "unconnected")
				{
				    window.ws.close();
				} else
				{
				    state = "connected";
				get_state_btn.disabled = false;
				    start_bluetooth_btn.disabled=false;

				}


			    };

			    // when the connection is closed, this method is called
			    window.ws.onclose = function () {
				state = "unconnected";


			    };
};