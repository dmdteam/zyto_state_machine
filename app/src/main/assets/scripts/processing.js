
function processing_setup()
{
var Interval = sessionStorage.getItem('Interval');
var V_Item = sessionStorage.getItem('Virtual_Item');
    sample_rate=100;
    update_interval=parseInt(Interval,10);
    num_intervals=parseInt(V_Item,10);

    T=1/sample_rate;
    len=15;

    u_i=update_interval;
    processing_reset();
    pnn_vals=[5,10,20,30,50];
}
function processing_SSF(x)
{
    delta_x=x-x_old;
    x_old=x;
    if(delta_x<=0)
    {
        delta_x=0;
    }
    new_sum=old_sum+delta_x-(old_sum/len);
    old_sum=new_sum;

    return new_sum;

}
function processing_reset()
{
    x_old=0;
    old_sum=0;
    new_sum=0;
    time=0;
    old_time=u_i;
    proc_array=[];
    ind_array=[];
    interval_count=0;

    base_RmSSD=0;

    base_pnn_results=[0,0,0,0,5];


}


function processing_process(x)
{
    time=time+T;
    x=processing_SSF(x);
    proc_array.push(x);
    if(time>u_i)
    {
    var temp=proc_array.shift();
    }
//    console.log("time"+ time+" old time: "+old_time+ "update_interval: "+u_i);
    if(time>old_time)
    {

        old_time+=u_i;

        var proc_x=proc_array.slice();
        peaks=findPeaks_win(proc_x,51);




        var RR=[];
        for(var i=1;i<peaks.length;i++)
        {
            var diff=(peaks[i]-peaks[i-1]);
            RR.push(diff);
        }
        var RR_sort=RR.slice();
        RR_sort.sort();
        median=RR_sort[Math.floor(RR_sort.length/2)];
//        console.log("T_start--------------------------- ");
//        console.log("Interval:"+interval_count+ " Number of intervals "+num_intervals+" time: "+time );

        RR = RR.filter(function(element){
            return element <element+median/2 && element>element-median/2;
        });

        var pnn_results= pnn_vals.map(function(num){
        return pnnx_calc(RR,num)
        } );
//        console.log("PNN results: "+pnn_results);
        var RMSSD=RMSSD_calc(RR)
//        console.log("RMSSD result: "+RMSSD);
        var stats=getStats(RR);
//        console.log("Stats: "+stats)
//        console.log("T_end--------------------------- ");
        RR.length=0;
        RR_sort.length=0;
        update_GUI(pnn_results,RMSSD,stats,interval_count);
        interval_count++;



    }
    if(interval_count==num_intervals)
            {
            stop_stream();
            console.log("Stopping stream");
            processing_reset();
            }

}

function update_GUI(pnn_results,RMSSD,stats,interval_count)
{
sessionStorage.setItem('PNNx',interval_count);
sessionStorage.setItem('PNNy1',pnn_results[0]);
sessionStorage.setItem('PNNy2',pnn_results[1]);
sessionStorage.setItem('PNNy3',pnn_results[2]);
sessionStorage.setItem('PNNy4',pnn_results[3]);
sessionStorage.setItem('PNNy5',pnn_results[4]);

sessionStorage.setItem('RMSSNx',interval_count);
sessionStorage.setItem('RMSSNy',RMSSD);

sessionStorage.setItem('AVG_R',stats[0]);
sessionStorage.setItem('MAX_R',stats[2]);
sessionStorage.setItem('MIN_R',stats[1]);
sessionStorage.setItem('STD_R',stats[3]);

if(interval_count==0)
{
    base_pnn_results=pnn_results.slice();
    base_RmSSD=RMSSD;

    sessionStorage.setItem('drNSSDx','0');
    sessionStorage.setItem('drNSSDy','0');

    sessionStorage.setItem('PNNbx','0');
    sessionStorage.setItem('PNNby1','0');
    sessionStorage.setItem('PNNby2','0');
    sessionStorage.setItem('PNNby3','0');
    sessionStorage.setItem('PNNby4','0');
    sessionStorage.setItem('PNNby5','0');
//    console.log("Base values");
//    console.log(base_RmSSD);
//    console.log(base_pnn_results);
//    console.log(base_stats);
}
else
{
sessionStorage.setItem('PNNbx',interval_count);
sessionStorage.setItem('PNNby1',pnn_results[0]-base_pnn_results[0]);
sessionStorage.setItem('PNNby2',pnn_results[1]-base_pnn_results[1]);
sessionStorage.setItem('PNNby3',pnn_results[2]-base_pnn_results[2]);
sessionStorage.setItem('PNNby4',pnn_results[3]-base_pnn_results[3]);
sessionStorage.setItem('PNNby5',pnn_results[4]-base_pnn_results[4]);

sessionStorage.setItem('drNSSDx',interval_count);
sessionStorage.setItem('drNSSDy',RMSSD-base_RmSSD);
base_pnn_results=pnn_results.slice();
    base_RmSSD=RMSSD;

}
}
function findPeaks_win(x,span)
	{
		output=[];
		var l_min, l_max, r_min, r_max;
		var i, j, k;
		var is_peak;
		var dist;
		dist = (span + 1) / 2;
		var n=x.length;

		for (i=0; i<n; i++)
		{
			l_min = Math.max(i-dist+1, 0);
			l_max = i-1;
			r_min = i+1;
			r_max = Math.min(i+dist-1, n-1);

			is_peak = 1;

			/* left side */
			for (j=l_min; j<=l_max; j++)
				if (x[j] >= x[i])
				{
					is_peak = 0;
					break;
				}

			/* right side */
			if (is_peak === 1)
				for (j=r_min; j<=r_max; j++)
					if (x[j] >= x[i])
					{
						is_peak = 0;
						break;
					}

			if (is_peak === 1)
			{
				output.push(i);
				//
			}
		}
		//    for(int p=0;p<output.size();p++)
		//    {
		//        System.out.println("Java Peak: "+output.get(p));
		//    }
		// build the output vector



		/* copy and free the list */
		return output;
	}
	function pnnx_calc(RR,X)
	{
	    var count=0;
	    for(var i=1;i<RR.length;i++)
	    {
	        if(Math.abs(RR[i]-RR[i-1])>X)
	        {
	            count++;
	        }
	    }
	    return Math.round(100*count/(RR.length-1)*100)/100;
	}
	function RMSSD_calc(RR)
	{
	    var mean_square=0;
	    for(var i=1;i<RR.length;i++)
	    {
	         mean_square+=Math.pow(RR[i]-RR[i-1],2);
	    }
	    mean_square=Math.sqrt(mean_square/(RR.length-1));
	    mean_square=Math.round(mean_square*100)/100;
	    return mean_square;


	}
	function getStats(RR)
	{
	 var sum=0;
	 var min=1000;
	 var max=-1000;
	 var std=0;
     var hr=RR.map(function(num){
                 	 return 60/(num*T);
                 	 });
	 for(var i=0;i<hr.length;i++)
	 {
	    if(hr[i]<min)
	    {
	    min=hr[i];
	    }
	    if(hr[i]>max)
	    {
	    max=hr[i];
	    }
	    sum=sum+hr[i];
	 }
	 sum=sum/hr.length;

	 for(var i=0;i<hr.length;i++)
     	 {
     	    std=std+Math.pow(hr[i]-sum,2)
     	 }
     	 std=Math.sqrt(std/hr.length);
     	 sum=Math.round(sum);
     	 min=Math.round(min);
     	 max=Math.round(max);
     	 std=Math.round(std*100)/100;

     	 var results=[sum,min,max,std];



     return results;

	}
